FROM python:3

ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

COPY . /todo_project/
WORKDIR /todo_project
COPY requirements.txt /todo_project/
RUN pip install -r requirements.txt


COPY . /entrypoint.sh/
ENTRYPOINT ["sh", "entrypoint.sh"]
